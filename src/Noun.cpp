//
// Created by root on 9/26/18.
//

#include "../include/Noun.h"

Noun::Noun(std::string local, std::string foreign)
{
    ForeignFormat = "[article] [translation],- [plural] (plural can be marked with : for umlaut as in \":er\")";
    LocalFormat = "[translation]";
    Local = local;
    Foreign = foreign;
}