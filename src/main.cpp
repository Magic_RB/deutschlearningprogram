#include <iostream>
#include <random>
#include <vector>
#include <functional>
#include "../include/Word.h"
#include "../include/Noun.h"
#include "../include/Scoreboard.h"

int main() {
    Scoreboard scoreboard = Scoreboard();

    std::vector<Word> wordVector;

    //casti tela

    wordVector.push_back(Noun("telo", "der Ko:rper,- s"));
    wordVector.push_back(Noun("tvar", "das Gesicht,- er"));
    wordVector.push_back(Noun("hlava", "der Kopf,- :e"));
    wordVector.push_back(Noun("celo", "die Stirn,- en"));
    wordVector.push_back(Noun("oko", "das Auge,- n"));
    wordVector.push_back(Noun("nos", "die Nase,- n"));
    wordVector.push_back(Noun("ucho", "das Ohr,- en"));
    wordVector.push_back(Noun("lice", "die Wange,- n"));
    wordVector.push_back(Noun("usta", "der Mund,- :er"));
    wordVector.push_back(Noun("pera", "die Lippe,- n"));
    wordVector.push_back(Noun("brada", "das Kinn,- e"));
    wordVector.push_back(Noun("zub", "der Zahn,- :e"));
    wordVector.push_back(Noun("jazyk", "die Zu:nge,- n"));
    wordVector.push_back(Noun("vlas", "das Haar,- e"));
    wordVector.push_back(Noun("krk", "der Hals,- :e"));
    wordVector.push_back(Noun("rameno", "die Schulter,- n"));
    wordVector.push_back(Noun("ruka", "der Arm,- e"));
    wordVector.push_back(Noun("ruka", "die Hand,- :e"));
    wordVector.push_back(Noun("hrud", "die Brust,- :e"));
    wordVector.push_back(Noun("brucho", "der Bauch,- :e"));
    wordVector.push_back(Noun("chrbat", "der Ru:cken,-"));
    wordVector.push_back(Noun("noha", "das Bein,- e"));
    wordVector.push_back(Noun("chodidlo", "der Fuss,- :e"));
    wordVector.push_back(Noun("prst", "der Finger,-"));
    wordVector.push_back(Noun("palec", "der Daumen,-"));
    wordVector.push_back(Noun("prst na nohe", "der Zeh,- en"));


    // Oblecenie
    wordVector.push_back(Noun("oblecenie", "die Kleidung,-"));
    wordVector.push_back(Noun("sukna", "der Rock,- :e"));
    wordVector.push_back(Noun("kabat", "der Mantel,- :"));
    wordVector.push_back(Noun("bunda", "die Jacke,- n"));
    wordVector.push_back(Noun("bluzka", "die Bluse,- n"));
    wordVector.push_back(Noun("nohavice", "die Hose,- n"));
    wordVector.push_back(Noun("okuliare", "die Brille,- n"));
    wordVector.push_back(Noun("topanka", "der Schuh,- e"));
    wordVector.push_back(Noun("rukavica", "der Handschuh,- e"));
    wordVector.push_back(Noun("saty", "das Kleid,- er"));
    wordVector.push_back(Noun("rifle", "die Jeans,-"));
    wordVector.push_back(Noun("kosela", "das Hemd,- en"));
    wordVector.push_back(Noun("sveter", "der Pullover,-"));
    wordVector.push_back(Noun("ciapka", "die Mu:tze,- n"));
    wordVector.push_back(Noun("klobuk", "der Hut,- :e"));
    wordVector.push_back(Noun("tricko", "das T-shirt,- s"));
    wordVector.push_back(Noun("oblek", "der Anzug,- :e"));
    wordVector.push_back(Noun("kabar", "der Blaser,-"));
    wordVector.push_back(Noun("sako", "der/das Sakko,- s"));
    wordVector.push_back(Noun("kravata", "die Kravatte,- n"));
    wordVector.push_back(Noun("motylik", "die Fliege,- n"));
    wordVector.push_back(Noun("vesta", "die Veste,- n"));
    wordVector.push_back(Noun("opasok", "der Gu:rtel,-"));
    wordVector.push_back(Noun("teniska", "der Sportschuh,- e"));
    wordVector.push_back(Noun("cizma", "der Stiefel,-"));
    wordVector.push_back(Noun("sandal", "die Sandale,- n"));
    wordVector.push_back(Noun("ponozky", "die Socke,- n"));
    wordVector.push_back(Noun("spodne pradlo", "die Unterwa:sche,- n"));
    wordVector.push_back(Noun("pancuchy", "die Strumpfhose,- n"));
    wordVector.push_back(Noun("kozuch", "der Pelz,- e"));
    wordVector.push_back(Noun("kabat 2", "der Anorak,- s"));
    wordVector.push_back(Noun("plavky F", "der Badeanzug,- :e"));
    wordVector.push_back(Noun("plavky M", "die Badenhose,- n"));
    wordVector.push_back(Noun("nausnica", "der Ohrring,- e"));
    wordVector.push_back(Noun("nahrdelnik", "die Kettle,- n"));
    wordVector.push_back(Noun("prsten", "der Ring,- e"));
    wordVector.push_back(Noun("hodinky", "der Armbanduhr,- en"));

    // vlastnosti
    wordVector.push_back(Noun("nudny", "langweilig"));
    wordVector.push_back(Noun("bohaty", "reich"));
    wordVector.push_back(Noun("verny", "treu"));
    wordVector.push_back(Noun("zdravy", "ges:under"));
    wordVector.push_back(Noun("mudry", "klug"));
    wordVector.push_back(Noun("usilovny", "fleissig"));
    wordVector.push_back(Noun("pracovity", "arbeitswillig"));
    wordVector.push_back(Noun("panovacny", "dominant"));
    wordVector.push_back(Noun("laskavy", "liebevolt"));
    wordVector.push_back(Noun("tvrdohlavy", "hartk:opfig"));
    wordVector.push_back(Noun("cestny", "ehrlich"));
    wordVector.push_back(Noun("pohodlny", "bequem"));
    wordVector.push_back(Noun("prijemny", "angenehmer"));
    wordVector.push_back(Noun("vtipny", "witzig"));
    wordVector.push_back(Noun("vlastnos", "die Eigenschaft,- en"));
    wordVector.push_back(Noun("pokojny", "ruhig"));
    wordVector.push_back(Noun("pritelsky", "freundlich"));
    wordVector.push_back(Noun("sympaticky", "sympatish"));
    wordVector.push_back(Noun("egoisticky", "selksts:uehtig"));
    wordVector.push_back(Noun("neuprimny", "unoufriehtig"));
    wordVector.push_back(Noun("mily", "nett"));
    wordVector.push_back(Noun("vtipny", "lustig"));
    wordVector.push_back(Noun("prisny", "streug"));
    wordVector.push_back(Noun("opatrny", "vorsichtig"));
    wordVector.push_back(Noun("poriadny", "ordentlich"));
    wordVector.push_back(Noun("lenivy", "faul"));
    wordVector.push_back(Noun("zabavny", "am:asand"));
    wordVector.push_back(Noun("tolerantly", "tolerant"));

    std::string count;

    std::cout << "How many questions do you want? " << std::flush;
    getline(std::cin, count);

    std::random_device r;
    std::default_random_engine generator(r());
    std::uniform_int_distribution<unsigned int> distribution(0, (unsigned int)wordVector.size() - 1);
    auto dice = std::bind (distribution, generator);

    bool skip_roll = false;
    Word* current_word;

    for (int i = 0; i < atoi(count.c_str()); i++) {
        if (!skip_roll)
            current_word = &wordVector.at((unsigned long) dice());

        std::cout << current_word->Local << " = " << std::flush;
        std::string answer;
        getline(std::cin, answer);


        if (answer == current_word->Foreign) {
            std::cout << "ok" << std::endl;
            scoreboard.AddAnswer(true);
        } else if (answer == "?format") {
                std::cout << current_word->LocalFormat << " = " << current_word->ForeignFormat << std::endl;

                i--;
                skip_roll = true;
        } else {
            std::cout << "wrong " << current_word->Foreign << std::endl;
            scoreboard.AddAnswer(false);
        }
    }

    std::cout << "Your total score is: " << scoreboard.CorrectAnswers << "/" << scoreboard.TotalAnswers << " or " << scoreboard.CorrectAnswers / (scoreboard.TotalAnswers / 100.0f) << "%" << std::endl;
}