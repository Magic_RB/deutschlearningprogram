//
// Created by root on 9/26/18.
//

#include "../include/Scoreboard.h"

void Scoreboard::AddAnswer(bool correct)
{
    TotalAnswers += 1;
    if (correct)
        CorrectAnswers += 1;
    else
        WrongAnswers += 1;
}