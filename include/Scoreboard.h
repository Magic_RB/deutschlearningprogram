//
// Created by root on 9/26/18.
//

#ifndef DEUTSCH_LEARNING_PROGRAM_SCOREBOARD_H
#define DEUTSCH_LEARNING_PROGRAM_SCOREBOARD_H


class Scoreboard {
public:
    int CorrectAnswers;
    int WrongAnswers;
    int TotalAnswers;

    void AddAnswer(bool correct);
};


#endif //DEUTSCH_LEARNING_PROGRAM_SCOREBOARD_H
