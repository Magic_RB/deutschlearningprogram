//
// Created by root on 9/26/18.
//

#ifndef DEUTSCH_LEARNING_PROGRAM_NOUN_H
#define DEUTSCH_LEARNING_PROGRAM_NOUN_H


#include "Word.h"

class Noun : public Word {
public:
    Noun(std::string local, std::string foreign);
};


#endif //DEUTSCH_LEARNING_PROGRAM_NOUN_H
