//
// Created by root on 9/26/18.
//

#ifndef DEUTSCH_LEARNING_PROGRAM_WORD_H
#define DEUTSCH_LEARNING_PROGRAM_WORD_H


#include <string>

class Word {
public:
    std::string Foreign;
    std::string Local;

    std::string ForeignFormat;
    std::string LocalFormat;
};


#endif //DEUTSCH_LEARNING_PROGRAM_WORD_H
